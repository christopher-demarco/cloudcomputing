# cloudhome

On-demand create & configure a cloud workstation.

## Even an iPad can do it!

Login to AWS and launch . . . 

  - For the first time?
     - Generate a key locally.
     - Launch the instance with [defaults](#defaults).
  - Returning?
     - Resume instance, update firewall.

SSH in.

## userland
Post-boot config management is kickoffed by cicd. 

How do you authenticate to your services, e.g. Dropbox?


## dev env
### Create an AMI from scratch:
(This is dumb because hopefully you never do it.)

Boot Ubuntu 20; ssh in.

```
sudo useradd -G sudo -m <username>
sudo passwd <username>
sudo su - <username>
mkdir .ssh && chmod 0700 ~/.ssh && cat > ~/.ssh/authorized_keys
# (paste ssh pubkey used for workstation --> this instance)
cat > ~/.ssh/cloudhome
# (paste ssh privkey used for this instance --> gitlab)
chmod 0600 ~/.ssh/*

logout, ssh as <username>

sudo apt update && sudo apt install awscli
mkdir -p cmd/src ; cd cmd/src
git clone git@gitlab.com:christopher-demarco/cloudcomputing.git
git clone git@gitlab.com:christopher-demarco/cloudcomputing.wiki.git
mkdir ~/.aws && cat > ~/.aws/credentials
# (paste aws creds)

# WARNING
# Now that the instance has creds and privkey, it is UNSAFE to make an ami!
# This really needs to be some kind of 2fa or ephemeral token

aws ec2 create-image --instance-id $(wget -q -O - http://169.254.169.254/latest/meta-data/instance-id) --name nimbus-seed --region us-west-1 --no-reboot

```


## defaults
TODO

# KNOWN ISSUES

- Naive aws creds
